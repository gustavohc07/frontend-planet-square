# PlanetSquare

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

## Como rodar o projeto

O projeto em questão trata-se apenas do front end. Para vê-lo em funcionamento é necessário que inicialize o servidor.

Para fazer isso acesse [Backend](https://gitlab.com/gustavohc07/backend-planet-square), clone o projeto e inicialize o servidor
com o seguinte comando:

``` ./mvnw spring-boot:run ```

Como backend rodando, basta executar o comando ng `ng serve` e acessar 
`http://localhost:4200/`.


## Funcionalidades

O usuário pode ver todos os planetas listados, adicionar planeta, ver detalhes de um planeta, editar um planeta e excluir um planeta.


