import { Component, OnInit, NgZone } from '@angular/core';
import { PlanetService } from '../shared/planet/planet.service'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'app-new-planet',
  templateUrl: './new-planet.component.html',
  styleUrls: ['./new-planet.component.css']
})
export class NewPlanetComponent implements OnInit {
  planetForm: FormGroup;
  PlanetArr: any = []

  ngOnInit() {
    this.addPlanet()
  }

  constructor(
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public planetService: PlanetService
  ) { }

  addPlanet() {
    this.planetForm = this.fb.group({
      name: [''],
      description: [''],
      image_url: [''],
      radius: [''],
      water: [''],
      discovery: ['']
    })
  }

  submitForm() {
    this.planetService.CreatePlanet(this.planetForm.value).subscribe(res => {
      console.log('Planet added!')
      this.ngZone.run(() => this.router.navigateByUrl('/planets'))
    })
  }
}
