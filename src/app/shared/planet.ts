export class Planet {
  id: string;
  name: string;
  description: string;
  image_url: string;
  radius: number;
  water: boolean;
  discovery: Date;
}