import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Planet } from '../planet'
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class PlanetService {
  public API = '//localhost:8080'
  public PLANET_API = this.API + '/planets'

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    'responseType': 'text' as 'json'
  }

  // Post
  CreatePlanet(data): Observable<Planet> {
    console.log(JSON.stringify(data));
    return this.http.post<Planet>(this.PLANET_API + '/new', JSON.stringify(data), this.httpOptions)
  }

  // GET

  GetPlanet(id): Observable<Planet> {
    return this.http.get<Planet>(this.PLANET_API + '/' + id)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // GET

  GetPlanets(): Observable<Planet> {
    return this.http.get<Planet>(this.PLANET_API)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // PUT

  UpdatePlanet(id, data): Observable<Planet> {
    return this.http.put<Planet>(this.PLANET_API + '/'+ id + '/edit', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  DeletePlanet(id){
    return this.http.delete<Planet>(this.PLANET_API + '/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\n Message: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
