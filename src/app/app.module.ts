import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'

import { PlanetListComponent } from './planet-list/planet-list.component'; 
import { PlanetShowComponent } from './planet-show/planet-show.component';
import { PlanetEditComponent } from './planet-edit/planet-edit.component';

import { CardModule } from 'primeng/card';
import { MenubarModule } from 'primeng/menubar';
import { MenubarComponent } from './menubar/menubar.component';
import { ButtonModule } from 'primeng/button';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { NewPlanetComponent } from './new-planet/new-planet.component';
import { PlanetService } from './shared/planet/planet.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  declarations: [
    AppComponent,
    PlanetListComponent,
    MenubarComponent,
    PlanetShowComponent,
    PlanetEditComponent,
    NewPlanetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    MenubarModule,
    ButtonModule,
    InputSwitchModule,
    CalendarModule,
    InputTextModule,
    InputTextareaModule,
    BrowserAnimationsModule,
  ],
  providers: [PlanetService],
  bootstrap: [AppComponent]
})
export class AppModule { }

