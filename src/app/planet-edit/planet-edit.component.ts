import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { PlanetService } from '../shared/planet/planet.service'
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-planet-edit',
  templateUrl: './planet-edit.component.html',
  styleUrls: ['./planet-edit.component.css']
})
export class PlanetEditComponent implements OnInit {
  PlanetsList: any = []
  updatePlanetForm: FormGroup;

  constructor(private actRoute: ActivatedRoute,
    public planetService: PlanetService,
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router) {
      var id = this.actRoute.snapshot.paramMap.get('id');
      this.planetService.GetPlanet(id).subscribe((data) => {
        this.updatePlanetForm = this.fb.group({
          name: [data.name],
          description: [data.description],
          image_url: [data.image_url],
          radius: [data.radius],
          water: [data.water],
          discovery: [data.discovery]
        })
      })
     }

  ngOnInit() {
    this.updateForm()
  }

  updateForm() {
    this.updatePlanetForm = this.fb.group({
      name: [''],
      description: [''],
      image_url: [''],
      radius: [''],
      water: [''],
      discovery: ['']
    })
  }

  submitForm() {
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.planetService.UpdatePlanet(id, this.updatePlanetForm.value).subscribe(res => {
      this.ngZone.run(() => this.router.navigateByUrl('/planets'))
    })
  }
  
}
