import { Component, OnInit } from '@angular/core';
import { PlanetService } from '../shared/planet/planet.service';

@Component({
  selector: 'app-planet-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.css']
})
export class PlanetListComponent implements OnInit {
  
  Planets: any = []
  
  constructor(public planetService: PlanetService) { }

  ngOnInit(): void {
    this.planetService.GetPlanets().subscribe(data => {
      this.Planets = data;
    })
  }

}
