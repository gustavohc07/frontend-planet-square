import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetListComponent } from './planet-list/planet-list.component'
import { PlanetShowComponent } from './planet-show/planet-show.component'
import { PlanetEditComponent } from './planet-edit/planet-edit.component'
import { NewPlanetComponent } from './new-planet/new-planet.component'

const routes: Routes = [
  { path: '', redirectTo: '/planets', pathMatch: 'full' },
  { 
    path: 'planets',
    component: PlanetListComponent
  },
  {
    path: 'planets/new',
    component: NewPlanetComponent
  },
  {
    path: 'planets/:id',
    component: PlanetShowComponent
  },
  {
    path: 'planets/:id/edit',
    component: PlanetEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
