import { Component, OnInit } from '@angular/core';
import { MenuItem } from "primeng/api"

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.css']
})
export class MenubarComponent {

  public items: MenuItem[];
  ngOnInit(): void {
    this.items = [
      {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        routerLink: '/planets'
      },
      {
        label: 'Planetas',
        items: [ { label: 'Criar Planeta', icon: 'pi pi-plus', routerLink: '/planets/new' } ]
      }
    ]
  }
}
