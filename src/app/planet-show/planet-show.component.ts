import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { PlanetService } from '../shared/planet/planet.service'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-planet-show',
  templateUrl: './planet-show.component.html',
  styleUrls: ['./planet-show.component.css']
})
export class PlanetShowComponent implements OnInit {
planet: any = [];

sub: Subscription;

  constructor(private route: ActivatedRoute,
              private planetService: PlanetService,
              private router: Router) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.planetService.GetPlanet(id).subscribe((planet: any) => {
          if (planet) {
            this.planet = planet;
            this.planet.href = planet._links.self.href;
          } 
        });
      }
    });
  }

  deletePlanet(data) {
    this.planetService.DeletePlanet(data.id).subscribe(res => {
      this.router.navigate(['/planets']);
      console.log('Planet deleted!')
    })
  }

}
